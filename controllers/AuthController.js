var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var User = require('../models/UserModel');

module.exports = {
    generateToken: function(user) {
        var token = jwt.sign(user, process.env.secret , {
            expiresIn: 86400 // expires in 24 hours
        });
        return token;
    },
    checkAuthentication: function (req, res, next) {
        var token = req.headers.authorization;
        console.log(req.headers.authorization);
        if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
        
        jwt.verify(token, process.env.secret, function(err, decoded) {
            if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' }); 
            req.user = decoded;
            next();
        });
    }
};