var UserModel = require('../models/UserModel.js');
var AuthController = require('./AuthController');
/**
 * UserController.js
 *
 * @description :: Server-side logic for managing Users.
 */
module.exports = {

    /**
     * UserController.list()
     */
    list: function (req, res) {
        UserModel.find(function (err, Users) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting User.',
                    error: err
                });
            }
            return res.json(Users);
        });
    },

    /**
     * UserController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        UserModel.findOne({_id: id}, function (err, User) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting User.',
                    error: err
                });
            }
            if (!User) {
                return res.status(404).json({
                    message: 'No such User'
                });
            }
            return res.json(User);
        });
    },

    /**
     * UserController.login()
     */
    login: function (req, res) {
        var reqParams = {email: req.body.email,mobile: req.body.mobile};
        UserModel.findOne(reqParams, function (err, User) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting User.',
                    error: err
                });
            }
            if (!User) {
                return res.status(404).json({
                    message: 'No such User'
                });
            }
            var token = AuthController.generateToken(User.toJSON());
            return res.json({token:token,user:User});
        });
    },
    /**
     * UserController.create()
     */
    create: function (req, res) {
        var User = new UserModel({
			firstName : req.body.firstName,
			lastName : req.body.lastName,
			dob : req.body.dob,
			mobile : req.body.mobile,
			email : req.body.email,
			country : req.body.country

        });

        User.save(function (err, User) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating User',
                    error: err
                });
            }
            var token = AuthController.generateToken(User.toJSON());
            return res.json({token:token,user:User});
        });
    },

    /**
     * UserController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        UserModel.findOne({_id: id}, function (err, User) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting User',
                    error: err
                });
            }
            if (!User) {
                return res.status(404).json({
                    message: 'No such User'
                });
            }

            User.firstName = req.body.firstName ? req.body.firstName : User.firstName;
			User.lastName = req.body.lastName ? req.body.lastName : User.lastName;
			User.dob = req.body.dob ? req.body.dob : User.dob;
			User.mobile = req.body.mobile ? req.body.mobile : User.mobile;
			User.email = req.body.email ? req.body.email : User.email;
			User.country = req.body.country ? req.body.country : User.country;
			
            User.save(function (err, User) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating User.',
                        error: err
                    });
                }

                return res.json(User);
            });
        });
    },

    /**
     * UserController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        UserModel.findByIdAndRemove(id, function (err, User) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the User.',
                    error: err
                });
            }
            return res.status(200).json({
                message: 'Successfully deleted the User.',
            });
        });
    }
};
