var TempUserLoginModel = require('../models/TempUserLoginModel.js');

/**
 * TempUserLoginController.js
 *
 * @description :: Server-side logic for managing TempUserLogins.
 */
module.exports = {

    /**
     * TempUserLoginController.list()
     */
    list: function (req, res) {
        TempUserLoginModel.find(function (err, TempUserLogins) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting TempUserLogin.',
                    error: err
                });
            }
            return res.json(TempUserLogins);
        });
    },

    /**
     * TempUserLoginController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        TempUserLoginModel.findOne({_id: id}, function (err, TempUserLogin) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting TempUserLogin.',
                    error: err
                });
            }
            if (!TempUserLogin) {
                return res.status(404).json({
                    message: 'No such TempUserLogin'
                });
            }
            return res.json(TempUserLogin);
        });
    },

    /**
     * TempUserLoginController.create()
     */
    create: function (req, res) {
        var TempUserLogin = new TempUserLoginModel({
			username : req.body.username,
			password : req.body.password

        });

        TempUserLogin.save(function (err, TempUserLogin) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating TempUserLogin',
                    error: err
                });
            }
            return res.status(201).json(TempUserLogin);
        });
    },

    /**
     * TempUserLoginController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        TempUserLoginModel.findOne({_id: id}, function (err, TempUserLogin) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting TempUserLogin',
                    error: err
                });
            }
            if (!TempUserLogin) {
                return res.status(404).json({
                    message: 'No such TempUserLogin'
                });
            }

            TempUserLogin.username = req.body.username ? req.body.username : TempUserLogin.username;
			TempUserLogin.password = req.body.password ? req.body.password : TempUserLogin.password;
			
            TempUserLogin.save(function (err, TempUserLogin) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating TempUserLogin.',
                        error: err
                    });
                }

                return res.json(TempUserLogin);
            });
        });
    },

    /**
     * TempUserLoginController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        TempUserLoginModel.findByIdAndRemove(id, function (err, TempUserLogin) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the TempUserLogin.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
