var UserLoginModel = require('../models/UserLoginModel.js');
var mongoose = require('mongoose');
var nev = require('email-verification')(mongoose);
/**
 * UserLoginController.js
 *
 * @description :: Server-side logic for managing UserLogins.
 */
module.exports = {

    /**
     * UserLoginController.list()
     */
    list: function (req, res) {
        UserLoginModel.find(function (err, UserLogins) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting UserLogin.',
                    error: err
                });
            }
            return res.json(UserLogins);
        });
    },

    /**
     * UserLoginController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        UserLoginModel.findOne({_id: id}, function (err, UserLogin) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting UserLogin.',
                    error: err
                });
            }
            if (!UserLogin) {
                return res.status(404).json({
                    message: 'No such UserLogin'
                });
            }
            return res.json(UserLogin);
        });
    },

    /**
     * UserLoginController.create()
     */
    create: function (req, res) {
        var UserLogin = new UserLoginModel({
			email : req.body.email,
			pw : req.body.pw

        });

        UserLogin.save(function (err, UserLogin) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating UserLogin',
                    error: err
                });
            }
            return res.status(201).json(UserLogin);
        });
    },

    /**
     * UserLoginController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        UserLoginModel.findOne({_id: id}, function (err, UserLogin) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting UserLogin',
                    error: err
                });
            }
            if (!UserLogin) {
                return res.status(404).json({
                    message: 'No such UserLogin'
                });
            }

            UserLogin.email = req.body.email ? req.body.email : UserLogin.email;
			UserLogin.pw = req.body.pw ? req.body.pw : UserLogin.pw;
			
            UserLogin.save(function (err, UserLogin) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating UserLogin.',
                        error: err
                    });
                }

                return res.json(UserLogin);
            });
        });
    },

    /**
     * UserLoginController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        UserLoginModel.findByIdAndRemove(id, function (err, UserLogin) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the UserLogin.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },
    /**
     * UserLoginController.signup()
     */
    signup: function (req, res) {
        var email = req.body.email;

        // register button was clicked
        if (req.body.type === 'register') {
            var pw = req.body.pw;
            var newUser = new UserLogin({
                email: email,
                pw: pw
            });

            nev.createTempUser(newUser, function(err, existingPersistentUser, newTempUser) {
                if (err) {
                    return res.status(404).send('ERROR: creating temp user FAILED');
                }

                // user already exists in persistent collection
                if (existingPersistentUser) {
                    return res.json({
                        msg: 'You have already signed up and confirmed your account. Did you forget your password?'
                    });
                }

                // new user created
                if (newTempUser) {
                    var URL = newTempUser[nev.options.URLFieldName];

                    nev.sendVerificationEmail(email, URL, function(err, info) {
                        if (err) {
                            return res.status(404).send('ERROR: sending verification email FAILED');
                        }
                        res.json({
                            msg: 'An email has been sent to you. Please check it to verify your account.',
                            info: info
                        });
                    });

                    // user already exists in temporary collection!
                } else {
                    res.json({
                        msg: 'You have already signed up. Please check your email to verify your account.'
                    });
                }
            });

            // resend verification button was clicked
        } else {
            nev.resendVerificationEmail(email, function(err, userFound) {
                if (err) {
                    return res.status(404).send('ERROR: resending verification email FAILED');
                }
                if (userFound) {
                    res.json({
                        msg: 'An email has been sent to you, yet again. Please check it to verify your account.'
                    });
                } else {
                    res.json({
                        msg: 'Your verification code has expired. Please sign up again.'
                    });
                }
            });
        }

    }
};
