var express = require('express');
var router = express.Router();
var UserLoginController = require('../controllers/UserLoginController.js');

/*
 * GET
 */
router.get('/', UserLoginController.list);

/*
 * GET
 */
router.get('/:id', UserLoginController.show);

/*
 * POST
 */
router.post('/', UserLoginController.create);

/*
 * PUT
 */
router.put('/:id', UserLoginController.update);

/*
 * DELETE
 */
router.delete('/:id', UserLoginController.remove);

module.exports = router;
