var express = require('express');
var router = express.Router();

var userRoute = require('./UserRoutes');
var  UserController = require("../controllers/UserController");
var  UserLoginController = require("../controllers/UserLoginController");
var app = express();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/login', UserController.login);


//app.use('/users',userRoute);

module.exports = router;
