var express = require('express');
var router = express.Router();
var TempUserLoginController = require('../controllers/TempUserLoginController.js');

/*
 * GET
 */
router.get('/', TempUserLoginController.list);

/*
 * GET
 */
router.get('/:id', TempUserLoginController.show);

/*
 * POST
 */
router.post('/', TempUserLoginController.create);

/*
 * PUT
 */
router.put('/:id', TempUserLoginController.update);

/*
 * DELETE
 */
router.delete('/:id', TempUserLoginController.remove);

module.exports = router;
