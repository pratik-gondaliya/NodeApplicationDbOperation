var mongoose = require('mongoose');
var nev = require('email-verification')(mongoose);
var User = require('../models/UserLoginModel');

// sync version of hashing function
var myHasher = function(password, tempUserData, insertTempUser, callback) {
    var hash = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    return insertTempUser(hash, tempUserData, callback);
};

// async version of hashing function
myHasher = function(password, tempUserData, insertTempUser, callback) {
    bcrypt.genSalt(8, function(err, salt) {
        bcrypt.hash(password, salt, function(err, hash) {
            return insertTempUser(hash, tempUserData, callback);
        });
    });
};


module.exports={

    initialize:function(){
            nev.configure({
                persistentUserModel: User,
                expirationTime: 600, // 10 minutes

                verificationURL: 'http://localhost:3000/email-verification/${URL}',
                transportOptions: {
                    service: 'Gmail',
                    auth: {
                        user: 'pratik.gondaliya@volansystech.com',
                        pass: '#Shivam1#'
                    }
                },

                hashingFunction: myHasher,
                passwordFieldName: 'pw',
            }, function(err, options) {
                if (err) {
                    console.log(err);
                    return;
                }

                console.log('configured: ' + (typeof options === 'object'));
            });

            nev.generateTempUserModel(User, function(err, tempUserModel) {
                if (err) {
                    console.log(err);
                    return;
                }

                console.log('generated temp user model: ' + (typeof tempUserModel === 'function'));
            });
        }
};