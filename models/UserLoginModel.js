var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema   = mongoose.Schema;

var UserLoginSchema = new Schema({
	'email' : String,
	'pw' : String
});

UserLoginSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.pw);
};

module.exports = mongoose.model('UserLogin', UserLoginSchema);

