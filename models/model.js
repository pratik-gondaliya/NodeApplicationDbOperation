var users = require("./UserModel");


module.exports.initialize = function () {
    return {
        users: users(),
    };
};