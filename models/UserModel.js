var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var UserSchema = new Schema({
    "firstName": {
      type: String,
      required: true,
      trim: true
    },
    "lastName": {
      type: String,
      required: true,
      trim: true
    },
    "dob": {
      type: Date      
    },
    "email": {
      type: String,
      required: true,
      trim: true
    },
    "mobile": {
      type: Number,
      unique: true,
	  required: true,
      trim: true,
      index: { unique: true }
    },
    "country": {
      type: String,
      trim: true
    }
});

module.exports = mongoose.model('User', UserSchema);
