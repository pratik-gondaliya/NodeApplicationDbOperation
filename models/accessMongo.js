'use strict';
// Module dependencies
var mongoose = require('mongoose');
var model = require('./model');
// connect to database
module.exports = {
    // initialize DB
    start: function (dbName) {
        mongoose.connect(dbName);
        // Check connection to mongoDB
        mongoose.connection.on('open', function () {
            console.log('We have connected to mongodb : ' + dbName);
            model.initialize();
            console.log('We have initialized models');
        });

    },
    
    // disconnect from database
    closeDatabase: function () {
        mongoose.disconnect();
    }
};
