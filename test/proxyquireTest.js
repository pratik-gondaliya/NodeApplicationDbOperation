var proxyquire = require('proxyquire');
var sinon = require('sinon');
var assert = require('assert');
var pathStub = {};

describe('SinonTest', function() {
  it('SPY() => should call the callback function once', function() {
    // when no overrides are specified, path.extname behaves normally
    var overridenModule = proxyquire('../controllers/ProxyquireController', { 'path': pathStub });
    assert.equal(overridenModule.extnameAllCaps('file.txt'), '.TXT');

    // override path.extname function
    pathStub.extname = function (file) { return 'Overriden method => ' + file; };

    // path.extname now behaves as we told it to
    assert.equal(overridenModule.extnameAllCaps('file.txt'), 'OVERRIDEN METHOD => FILE.TXT');

    // path.basename and all other path module methods still function as before
    assert.equal(overridenModule.basenameAllCaps('/a/b/file.txt'), 'FILE.TXT');
  });

});