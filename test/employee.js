//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let UserModel = require('../models/UserModel');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

//Our parent block
describe('Employees', () => {
    beforeEach((done) => { //Before each test we empty the database
        UserModel.remove({}, (err) => { 
           done();         
        });     
    });
/*
  * Test the /GET route
  */
  describe('/GET Users', () => {
      it('it should GET all the users', (done) => {
        chai.request(server)
            .get('/users')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
              done();
            });
      });
  });

 /*
  * Test the /POST route
  */
  describe('/POST Users',() => {
      it('it should not POST a users without mobile number field', (done) => {
            let user = {
                firstName: "Pratik",
                lastName: "Gondaliya",
                dob: '2-NOV-1991',
                email: 'gondaliya.p001@gmail.com'
            }

			chai.request(server)
		    .post('/users')
		    .send(user)
		    .end((err, res) => {
			  	res.should.have.status(500);
			  	res.body.should.be.a('object');
                res.body.should.have.property('message').be.eql('Error when creating User');
		      done();
		    });
	  });
	  it('it should POST a User ', (done) => {
	  	let user = {
	  		firstName: "Pratik",
	  		lastName: "Gondaliya",
	  		dob: '2-NOV-1991',
	  		email: 'gondaliya.p001@gmail.com',
            mobile: 9033627990
	  	}
			chai.request(server)
		    .post('/users')
		    .send(user)
		    .end((err, res) => {
			  	res.should.have.status(200);
			  	res.body.should.be.a('object');
			  	res.body.should.have.property('token');
			  	res.body.user.should.have.property('firstName');
			  	res.body.user.should.have.property('lastName');
			  	res.body.user.should.have.property('dob');
			  	res.body.user.should.have.property('email');
                res.body.user.should.have.property('mobile');
		      done();
		    });
	  });
  });

  /*
  * Test the /GET/:id route
  */
  describe('/GET/:id users', () => {
	  it('it should GET a book by the given id', (done) => {
	  	let userModel = new UserModel({
	  		firstName: "Pratik",
	  		lastName: "Gondaliya",
	  		dob: '2-NOV-1991',
	  		email: 'gondaliya.p001@gmail.com',
            mobile: 9033627990
	  	});
	  	userModel.save((err, userModel) => {
	  		chai.request(server)
		    .get('/users/' + userModel.id)
		    .send(userModel)
		    .end((err, res) => {
			  	res.should.have.status(200);
			  	res.body.should.be.a('object');
			  	res.body.should.have.property('firstName');
			  	res.body.should.have.property('lastName');
			  	res.body.should.have.property('dob');
			  	res.body.should.have.property('email');
                res.body.should.have.property('mobile');
			  	res.body.should.have.property('_id').eql(userModel.id);
		      done();
		    });
	  	});
			
	  });
  });

   /*
  * Test the /PUT/:id route
  */
  describe('/PUT/:id book', () => {
	  it('it should UPDATE a user given the id', (done) => {
	  	let userModel = new UserModel({
	  		firstName: "Pratik",
	  		lastName: "Gondaliya",
	  		dob: '2-NOV-1991',
	  		email: 'gondaliya.p001@gmail.com',
            mobile: 9033627990
	  	});
	  	userModel.save((err, userModel) => {
	  		chai.request(server)
		    .put('/users/' + userModel.id)
		    .send({firstName:'Hashmukh'})
		    .end((err, res) => {
			  	res.should.have.status(200);
			  	res.body.should.be.a('object');
                res.body.should.have.property('firstName').eql('Hashmukh');
			  	res.body.should.have.property('lastName');
			  	res.body.should.have.property('dob');
			  	res.body.should.have.property('email');
                res.body.should.have.property('mobile');
			  	res.body.should.have.property('_id').eql(userModel.id);
		      done();
		    });
	  	});
	  });
  });


   /*
  * Test the /DELETE/:id route
  */
  describe('/DELETE/:id book', () => {
	  it('it should DELETE user given the id', (done) => {
	  	let userModel = new UserModel({
	  		firstName: "Pratik",
	  		lastName: "Gondaliya",
	  		dob: '2-NOV-1991',
	  		email: 'gondaliya.p001@gmail.com',
            mobile: 9033627990
	  	});
	  	userModel.save((err, userModel) => {
	  		chai.request(server)
		    .del('/users/' + userModel.id)
		    .send(userModel)
		    .end((err, res) => {
			  	res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eql('Successfully deleted the User.');
		      done();
		    });
	  	});
	  });
  });

});