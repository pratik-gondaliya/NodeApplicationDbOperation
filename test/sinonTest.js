var sinon = require('sinon');
var sinonController = require('../controllers/SinonController');

describe('SinonTest', function() {
  it('SPY() => should call the callback function once', function() {
    var callback = sinon.spy();
    var error = sinon.spy();

    sinonController.addition(5,15, callback,error);
    sinon.assert.calledOnce(callback);
    sinon.assert.notCalled(error);
  });


  it('Stub() => should call the callback function once', function() {
    var callback = sinon.stub();
    var error = sinon.stub();

    sinonController.addition(5,15, callback,error);
    sinon.assert.calledOnce(callback);
    sinon.assert.notCalled(error);
  });
});